from ariadne.utils import convert_camel_case_to_snake


def convert_to_snake_case(d):
    converted = {}
    for k, v in d.items():
        if isinstance(v, dict):
            v = convert_to_snake_case(v)
        converted[convert_camel_case_to_snake(k)] = v
    return converted
