import boto3
from django.conf import settings


def get_secret(secret_name, **kwargs):
    defaults = dict(
        region_name=settings.AWS_DEFAULT_REGION,
        aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
        aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
    )

    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        **{**defaults, **kwargs},
    )

    get_secret_value_response = client.get_secret_value(
        SecretId=secret_name
    )

    if 'SecretString' in get_secret_value_response:
        return get_secret_value_response['SecretString']
    else:
        return get_secret_value_response['SecretBinary']
