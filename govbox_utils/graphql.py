import requests

try:
    from django.conf import settings
except ImportError:
    settings = None

from . import utils


class GraphQLException(Exception):
    pass


def gql(
    url,
    query,
    headers={},
    variables={},
    auth_provider=None,
    convert_to_snake_case=True,
    raise_for_status=True,
    raise_for_error=True
):
    """ Perform a GraphQL request to the given endpoint.

    By default, the defaults raise exceptions for both non-2* HTTP status codes AND for
    any GraphQL errors that are returned.

    :returns: A tuple of the (data, errors) in the response.
    """

    if settings:
        headers = {
            'User-Agent': settings.USER_AGENT,
            **headers,
        }

    if auth_provider:
        headers = {
            **auth_provider(),
            **headers,
        }

    r = requests.post(
        url,
        headers=headers,
        json={
            'query': query,
            'variables': variables,
        },
    )
    if raise_for_status:
        r.raise_for_status()

    json = r.json()

    if convert_to_snake_case:
        json = utils.convert_to_snake_case(json)

    data, errors = json.get('data', None), json.get('errors', None)

    if raise_for_error and errors:
        raise GraphQLException(f'Error with query.\nErrors: {errors}')

    return data, errors


def client(endpoint, auth_provider, convert_to_snake_case=True):

    def _client(query, **kwargs):
        return gql(
            endpoint,
            query,
            auth_provider=auth_provider,
            convert_to_snake_case=convert_to_snake_case,
            **kwargs,
        )

    return _client
