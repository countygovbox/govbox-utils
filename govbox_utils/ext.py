import functools

from .jq import query
from .graphql import client


class EndpointDiscoveryError(Exception):
    """ A given target does not support a given endpoint. """
    pass


def endpoint_discovery(
    action,
    type='Query',
    target=None,
    auth_provider=None,
    should_raise=True,
):
    """" Before performing the wrapped function, attempt to check if the endpoint
    that is provided exists on the target.

    :endpoint: The GraphQL typename that is required to exist.
    :type: The GraphQL type of the request (Query | Mutation).
    :target: The URL of the GraphQL schema that is being inspected. This parameter
    may be a callable that returns the URL.
    :auth_provider: A callable that returns a dictionary of authentication headers if needed.
    :should_raise: Whether the discovery process should raise an error if the endpoint
    is not supported or return None.

    If no endpoint exists, throw EndpointDiscoveryError
    """

    gql = client(
        target,
        auth_provider={
            'User-Agent': 'GovBox Endpoint Discovery/v1.0',
            **(auth_provider() if auth_provider else {}),
        }
    )

    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            data, errors = gql(f"""
                query {{
                  __type(name: "{type}") {{
                    name
                    fields {{
                      name
                    }}
                  }}
                }}
            """)

            exists = any(
                action == field['name']
                for field in query(data, '__type.fields')
            )
            if not exists and should_raise:
                raise EndpointDiscoveryError(
                    f'Target {target} does not support required endpoint {action}'
                )
            elif not exists:
                return None

            return func(*args, **kwargs)
        return wrapper
    return decorator
