from django.conf import settings

from cryptography.fernet import Fernet


if getattr(settings, 'IDENTITIES_FERNET_KEY', None):
    fernet = Fernet(settings.IDENTITIES_FERNET_KEY)
else:
    print(
        'WARNING: Fernet Encryption is using a temporary key!\n\n'
        'Any data encrypted this way will be unusable when the application restarts. '
        'To fix this, set the IDENTITIES_FERNET_KEY value in your Django settings.'
    )
    fernet = Fernet(Fernet.generate_key())


class IdentityEncoder:
    """ A utility class that provides the ability to use encrypted identities
    as a Unique Identifier rather than returning confidential or identifying information
    to GovBox.

    Identity Encoders hide the identifying information from the GovBox system but still
    allow a given DTL application to later identify the record(s) (or relationships)
    being discussed.

    To use this functionality, subclass IdentityEncoder and add in a unique value for
    your source system by overriding `source_id` and then, for each type of
    resource you wish to identify, either supply a `type` argument when initializing
    the encoder, or override that class with a default type.

    Encoders will validate that they can/should decode different types of identities
    depending on the `source_id` and `type` values.

    Identities are encrypted in production by default, but not in development. To enable
    encrypted identities in development, change the ENCRYPT_IDENTITIES setting or use set
    `ENCRYPT_IDENTITIES=true` before running the application.

    Example
    -------

        class AccountsSystemIdentityEncoder(IdentityEncoder):
            source_id = 'my-accounts-system'

    Usage
    -----

        class UserAccountIdentityEncoder(AccountsSystemIdentityEncoder):
            default_type = 'user-account'

        identity = UserAccountIdentityEncoder([user_id, user_name])

    *OR*

        identity = AccountsSystemIdentityEncoder([
            user_id,
            user_name,
        ], type='user-account')
    """

    source_id = 'unknown'
    partition = '::'
    default_type = None
    default_ttl = None

    class IdentityDecodeError(Exception):
        pass

    def __init__(self, values=[], type=None, cypher=None):
        self.cypher = cypher
        self.type = type if type else self.default_type
        self.values = [str(v) for v in values]

    def __str__(self):
        return self.encode()

    @property
    def use_encryption(self):
        return settings.ENCRYPT_IDENTITIES

    def encode(self):
        if self.cypher:
            return self.cypher

        encoded = self.partition.join([
            self.source_id,
            self.type,
            self.partition.join(self.values)
        ])

        if not self.use_encryption:
            return encoded

        return fernet.encrypt(encoded.encode('utf-8')).decode('utf-8')

    def decode(self, ttl=None):
        ttl = ttl if ttl else self.default_ttl
        try:
            identity = (
                fernet.decrypt(self.cypher.encode('utf-8'), ttl=ttl).decode('utf-8')
                if self.use_encryption
                else self.cypher
            )
            source_id, type, *rest = identity.split(self.partition)
        except Exception:
            raise IdentityEncoder.IdentityDecodeError(f'Unable to decode {self.cypher}')

        if source_id != self.source_id:
            raise ValueError(
                f'This token with source_id {source_id} does not correspond to a '
                f'{self.source_id} identity.'
            )

        if self.type and self.type != type:
            raise ValueError(
                f'Identity should represent {self.type} but is of type {type}'
            )
        self.type = type

        return rest
