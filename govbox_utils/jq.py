def get(data, q, default=None, should_raise=False):
    """ This method allows for a simple recursive queries on JSON fields,
    including Python's native dictionary.

    This function is intentionally modeled on lodash.get

    Sample query:
    -------------

    data = {
        'user' {
            'aliases': [
                {
                    'name': 'Joe'
                }, {
                    'name': 'Joey'
                }
            ]
        }
    }
    query(data, 'user.aliases.0.name')
    >>> 'Joe'
    """
    try:
        return _query(data, q)
    except (KeyError, ValueError, IndexError, TypeError) as e:
        if not should_raise:
            return default
        else:
            raise e


def _query(data, q):
    first, *rest = q.split('.', maxsplit=1)

    try:
        first = int(first)
    except ValueError:
        pass

    data = data[first]
    if rest:
        return _query(data, rest[0])
    else:
        return data
