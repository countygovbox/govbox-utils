import logging

from django.test import TestCase
from django.shortcuts import reverse


logger = logging.getLogger(__name__)


class GraphQLTestCase(TestCase):

    graphql_view_name = 'graphql'

    def gql(self, query, variables={}, **headers):
        response = self.client.post(
            reverse(self.graphql_view_name),
            content_type='application/json',
            **headers,
            data={
                'query': query,
                'variables': variables,
            },
        )

        if response.status_code != 200:
            logger.error(
                f'GraphQL returned non-200 status code.\n'
                f'Response Content:\n\n{response.content}\n'
            )
            self.assertEqual(response.status_code, 200)

        body = response.json()
        return body.get('data'), body.get('errors')
