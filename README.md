# GOVbox Utils

A utility belt for all GOVbox services.

## About

`govbox-utils` is a simple suite of tools that are useful in almost any application. This library provides functionality for Querying deep, nested dictionaries, Encoding Complex Govbox Identities, Writing GraphQL Unit Tests, and Querying GQL endpoints, etc.

Simply install this library to begin using the functionality.

This library is known to work on Python 3.7 but probably works fine in 3.8 as well.

## Installation

Installing this library is trivial.

```
$ pip install git+https://bitbucket.org/countygovbox/govbox-utils.git#egg=govbox_utils
```
