from setuptools import setup, find_packages
from os import path, chdir

from io import open

here = path.abspath(path.dirname(__file__))


with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


# allow setup.py to be run from any path
chdir(here)


setup(
    name='govbox-utils',
    version='1.0.0',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://bitbucket.org/countygovbox/govbox-utils',
    author='Brian Schrader',
    author_email='brian.schrader@sdcounty.ca.gov',
    classifiers=[
        'Development Status :: 1 - Alpha',

        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',

        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    packages=find_packages(),
    include_package_data=True,
    python_requires='>=3.7,',
    install_requires=[
        'cryptography',
        'requests',
        'boto3',
        'ariadne',
    ],
)
